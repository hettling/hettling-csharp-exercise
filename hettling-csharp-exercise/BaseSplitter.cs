﻿using System;
namespace hettling_csharp_exercise
{
    /// <summary>
    /// Base class for string splitting with abstract split method.
    /// Possible implementations could be split by a provided character,
    /// split based on keyword, split based on regex, etc.
    /// </summary>
    public abstract class BaseSplitter
    {

        /// <summary>
        /// Definition of split method implemented by the subclasses
        /// </summary>
        /// <param name="to_split"></param>
        /// <returns></returns>
        public abstract string[] Split(string to_split);

    }
}
