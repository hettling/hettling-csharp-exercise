﻿using System;

namespace hettling_csharp_exercise
{
    /// <summary>
    /// This class implements the main routine for the C# exercise.
    /// Note that the input file name is hard-coded. The input file is copied
    /// into the output directory that contains the executable.
    /// </summary>
    class CountIt
    {

        /// <summary>
        /// Main routine to execute the C# exercise
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {

            // specify input file name
            string input_file = "input.txt";

            // specify modifiers that
            // (i) select only 'real' words without numbers or whitespaces
            // (ii) sets all words to lowercase
            // (iii) sorts the words
            ItemModifierBase[] modifiers = { new SelectWordsModifier(),
                                             new ToLowerCaseModifier(),
                                             new QuickSortModifier() };

            // generate TextDocument object from our specs
            TextDocument document = new TextDocument(input_file,
                                                     new SimpleSplitter(),
                                                     modifiers);

            // print document statistics to console
            document.print_stats();

        }

    }
}
