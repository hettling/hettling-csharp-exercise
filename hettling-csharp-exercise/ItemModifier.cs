﻿namespace hettling_csharp_exercise
{
    /// <summary>
    /// Base class for "modifiers" that act on a list of string.
    /// Transformations can be of any kind, e.g. removing or adding elements,
    /// sorting, changing values for individual strings etc.
    /// </summary>
    public abstract class ItemModifierBase
    {
        /// <summary>
        /// "Apply" method that, for each subclass, implements a
        /// modification of an array of strings
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public abstract string[] apply(string[] items);
    }

}
