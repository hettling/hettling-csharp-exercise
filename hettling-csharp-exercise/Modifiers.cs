﻿using System;
using System.Linq;

namespace hettling_csharp_exercise
{
    /// <summary>
    /// Modifier class that acts on a list of strings and only selects
    /// those strings that are 'real' words: Thus no whitespace or number
    /// or special character in any string
    /// </summary>
    public class SelectWordsModifier : ItemModifierBase
    {

        public override string[] apply(string[] items)
        {
            Func<string, bool> validate = x => x.All(c => char.IsLetter(c));
            // remove strings that have only whitespace
            items = items.Where(s => !String.IsNullOrWhiteSpace(s)).ToArray();
            // remove strings that have non-letters
            items = items.Where(x => validate(x)).ToArray();

            return items;
        }
    }

    /// <summary>
    /// Modifier class that acts on a list of strings and turns them all to
    /// lowercase
    /// </summary>
    public class ToLowerCaseModifier : ItemModifierBase
    {
        public override string[] apply(string[] items)
        {
            return Array.ConvertAll(items, s => s.ToLower());
        }
    }

    /// <summary>
    /// Modifier class that acts on a list of strings and sorts them
    /// using the QuickSort algorithm
    /// </summary>
    public class QuickSortModifier : ItemModifierBase
    {
        string[] _items;


        public override string[] apply(string[] items)
        {
            _items = items;
            _QuickSort(0, items.Length - 1);
            return _items;
        }

        /// <summary>
        /// Basic implementation of QuickSort for strings
        /// </summary>
        /// <param name="idx_low"></param>
        /// <param name="idx_high"></param>
        private void _QuickSort(int idx_low, int idx_high)
        {
            int i = idx_low;
            int j = idx_high;
            String pivot = _items[idx_low + (idx_high - idx_low) / 2];
            while (i <= j) // recursion anchor
            {
                while (_items[i].CompareTo(pivot) < 0)
                {
                    i++;
                }

                while (_items[j].CompareTo(pivot) > 0)
                {
                    j--;
                }

                if (i <= j)
                {
                    _swapItems(i, j);
                    i++;
                    j--;
                }
            }
            // recursion
            if (idx_low < j)
            {
                _QuickSort(idx_low, j);
            }
            if (i < idx_high)
            {
                _QuickSort(i, idx_high);
            }
        }

        /// <summary>
        /// swap two items in member array _items given their
        /// indices in the array
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        void _swapItems(int i, int j)
        {
            String temp = _items[i];
            _items[i] = _items[j];
            _items[j] = temp;
        }

    }


}
