﻿using System;
namespace hettling_csharp_exercise
{

    /// <summary>
    /// Implementation of text splitter class implementing a simple split
    /// method where a String gets split based on separator characters.
    /// By default, split is done with separators " ", "."
    /// </summary>
    public class SimpleSplitter : BaseSplitter
    {
        /// <summary>
        /// Member holding the array of separators the input string is split on
        /// </summary>
        private char [] _separators;
     
        public SimpleSplitter(char [] separators = null)
        {
            // specify default Arguments
            separators = separators ?? new char[] { ' ', '.', '!', '?', ';' };
            _separators = separators;
        }

        public override string[] Split(string str)
        {
            return str.Split(_separators);
        }
    }
}
