﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace hettling_csharp_exercise
{
    /// <summary>
    /// Class representing a simple text document. The raw contents of the
    /// document are passed as a string. 
    /// The class provides functionality to split the string into an array
    /// of strings according to user-specified rules
    /// </summary>
    public class TextDocument
    {
        /// <summary>
        /// Member holding the raw document contents as a string.
        /// Member has a getter
        /// </summary>
        private string _raw_content;
        public string RawContent => _raw_content;

        /// <summary>
        /// Member for all individual items (e.g. words) in the document
        /// </summary>
        private string [] _items = null;
        public string[] Items => _items;


        /// <summary>
        /// Dictionary with keys being the individual items (_items) and
        /// the values being their frequency in this document
        /// </summary>
        private Dictionary<string, int> _item_counts;
        public Dictionary<string, int> ItemCounts => _item_counts;

        /// <summary>
        /// Constructor making a TextDocument given a filename
        /// Use dependency injection for filtering, selecting, sorting, etc.
        /// </summary>
        /// <param name="raw_content"></param>
        public TextDocument(string filename, BaseSplitter splitter,
            ItemModifierBase [] modifiers)
        {
            // read the items of this document and store in array of strings
            // TODO: Make async!!!
            _raw_content = System.IO.File.ReadAllText(filename);


            // split the string read from file with specified splitting object
            _items = splitter.Split(_raw_content);

            
            for (int i = 0; i < modifiers.Length; i++)
            {
                _items = modifiers[i].apply(_items);
            }

            // count the occurences of words and store in Dictionary
            _CountItems();

        }

        /// <summary>
        /// Print class statistics to console.
        /// First the total number of words, then
        /// all unique items of the document and their frequencies
        /// </summary>
        public void print_stats()
        {
            int total_count = _item_counts.Sum(x => x.Value);
            Console.WriteLine("Number of words: {0}\n", total_count);
            foreach (KeyValuePair<string, int> kv in _item_counts)
            {
                Console.WriteLine("{0} {1}", kv.Key, kv.Value);
            }
        }

        /// <summary>
        /// Count the frequencies of each item in this document
        /// and store the results in a Dictionary
        /// </summary>
        private void _CountItems()
        {
            _item_counts = _items.GroupBy(x => x).ToDictionary(
                group => group.Key,
                group => group.Count());
        }

    }
}
