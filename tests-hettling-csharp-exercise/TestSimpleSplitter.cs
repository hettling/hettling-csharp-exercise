﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using hettling_csharp_exercise;

namespace tests_hettling_csharp_exercise
{
    [TestClass]
    public class TestSimpleSplitter
    {
        [TestMethod]
        public void TestSplit()
        {
            // TODO: Many more extreme tests required!

            // test with default arguments
            SimpleSplitter sp = new SimpleSplitter();

            string to_split = "should be split";
            string[] correct = { "should", "be", "split"};
            string[] words = sp.Split(to_split);

            CollectionAssert.AreEqual(correct, words,
                "SimpleSplit works with default args");

            // test with custom arguments
            SimpleSplitter sp_custom = new SimpleSplitter(new char[] { '|' });
            to_split = "one|two";
            string [] correct_custom = { "one", "two" };
            string [] words_custom = sp_custom.Split(to_split);

            CollectionAssert.AreEqual(correct_custom, words_custom,
                "SimpleSplit works with custom arguments");

        }
    }
}
