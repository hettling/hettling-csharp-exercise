﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using hettling_csharp_exercise;

namespace tests_hettling_csharp_exercise
{
    [TestClass]
    public class TestTextDocument
    {
        [TestMethod]
        public void TestConstructor()
        {
            ItemModifierBase[] modifiers = { };
            string filename = "input.txt";

            TextDocument document = new TextDocument(filename,
                                         new SimpleSplitter(),
                                         modifiers);
            Assert.IsNotNull(document, "TextDocument constructor works");

        }
    }
}
