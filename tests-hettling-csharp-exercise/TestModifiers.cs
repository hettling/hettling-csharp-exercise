﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using hettling_csharp_exercise;

namespace tests_hettling_csharp_exercise
{
    [TestClass]
    public class TestSelectWordsModifier
    {
        [TestMethod]
        public void TestApply()
        {
            SelectWordsModifier mod = new SelectWordsModifier();
            string[] words = { "test1", "test2", "test", "1" };
            string[] expected = { "test" };
            string[] results = mod.apply(words);

            CollectionAssert.AreEqual(results, expected,
                   "SelectWordsModifier works");

        }
    }

    [TestClass]
    public class TestToLowerCaseModifier
    {
        [TestMethod]
        public void TestApply()
        {
            SelectWordsModifier mod = new SelectWordsModifier();
            string[] words = { "Aa", "Bb", "cC", "D" };
            string[] expected = { "aa", "bb", "cc", "d" };
            string[] results = mod.apply(words);

            CollectionAssert.AreEqual(results, expected,
                   "ToLowerCaseModifier works");

        }
    }

    [TestClass]
    public class TestQuickSortModifier
    {
        [TestMethod]
        public void TestApply()
        {
            SelectWordsModifier mod = new SelectWordsModifier();
            string[] words = { "C", "a", "dd", "bb" };
            string[] expected = { "a", "bb", "C", "dd" };
            string[] results = mod.apply(words);

            CollectionAssert.AreEqual(results, expected,
                   "QuickSortModifier works");

        }
    }

}
